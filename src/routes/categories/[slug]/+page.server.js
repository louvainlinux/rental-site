import {fetchContent} from "$lib/useFetch.js";

export const load = async ({ params }) => {
	return await fetchContent('GET', `api/category/inventories/${params.slug}`);
};

