function setQualityBool(qualityHard, qualitySoft) {
    if (qualityHard === true && qualitySoft === true) {
        return "Correct";
    } else if (qualityHard === false || qualitySoft === false) {
        return "Mauvais";
    } else {
        return "Inconnu";
    }
}

function setQualityBoolColors(qualityHard, qualitySoft) {
    if (qualityHard === true && qualitySoft === true) {
        return "bg-green-100 text-green-800";
    } else if (qualityHard === false || qualitySoft === false) {
        return "bg-red-100 text-red-800";
    } else {
        return "bg-orange-100 text-orange-800";
    }
}

export { setQualityBool, setQualityBoolColors }
