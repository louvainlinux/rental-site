import {fetchContent} from "$lib/useFetch.js";

export const load = async ({ params }) => {
	return await fetchContent('GET', `api/family/categories/${params.slug}`);
};

