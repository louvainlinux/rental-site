import {fetchContent} from "$lib/useFetch.js";

export const load = async ({}) => {
	return await fetchContent('GET', `api/families/`);
};
