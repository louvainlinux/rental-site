import { base, base_strapi } from "./useFetch.js";

function failedRequest(fetched) {
    if (fetched.status === 204) {
        fetched.message = "Not found: " + fetched.route
        return true
    }
    return 400 <= fetched.status && fetched.status < 600
}

function setIfNotUndefined(elem, defaultElem) {return elem === undefined ? defaultElem : elem}


function setPictureSrc(dataPicture) {
    return dataPicture !== null && dataPicture.url !== null ? (base_strapi + dataPicture.url) : ""
}

export { failedRequest, setIfNotUndefined, setPictureSrc }