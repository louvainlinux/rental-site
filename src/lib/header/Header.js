export let sections = [
    {   name: "Accueil",
        url: "/",
        target: "",
        current: false,
    },
    { name: "Nous contacter",
        url: "https://louvainlinux.org/nous-contacter",
        target: "_blank",
        current: false,
    },
]