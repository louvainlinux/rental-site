FROM node:18.9-alpine3.15
RUN npm install -g npm@8.19.2

# Import Environement Variables in the .env file
ARG VITE_INV_PATH
ENV VITE_INV_PATH=${VITE_INV_PATH}

# Set Environement Variables
ENV NODE_ENV_CMD=dev

WORKDIR /opt/app

COPY package.json .
RUN npm install

COPY ./ ./

EXPOSE 5173

CMD npm run ${NODE_ENV_CMD}