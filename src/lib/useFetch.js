import { env } from '$lib/env'
export const base = "http://localhost:5173"
export const base_strapi = env.llnuxInvPath;

/**
 * @param {string} method
 * @param {string} resource
 * @param {Record<string, unknown>} [data]
 */
export function useFetch(method, resource, data) {
    return fetch(`${base_strapi}/${resource}`, {
        method,
        headers: {
            'content-type': 'application/json'
        },
        body: data && JSON.stringify(data)
    });
}

export async function fetchContent(method, resource) {
    try {
        const response = await useFetch(method, resource);
        if (response.ok) {
            return {
                response: {
                    status: response.status,
                    message: response.statusText,
                },
                content: await response.json(),
            };
        } else {
            return {
                response: {
                    status: response.status,
                    message: response.statusText,
                },
                content: [],
            };
        }
    } catch {
        return undefined;
    }
}
