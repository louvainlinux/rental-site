function setQualityBool(quality) {
    return quality === null ? "Inconnu" : (quality ? "Correct" : "Mauvais");
}
function setQualityBoolColors(quality) {
    return quality === null ? "bg-orange-100 text-orange-800" : (quality ? "bg-green-100 text-green-800" : "bg-red-100 text-red-800");
}

export { setQualityBool, setQualityBoolColors }