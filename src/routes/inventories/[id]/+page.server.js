import {fetchContent} from "$lib/useFetch.js";

export const load = async ({ params }) => {
	return await fetchContent('GET', `api/inventories/${params.id}`);
};

